package controller;

import java.util.Scanner;
import java.util.function.Supplier;

import com.sun.javafx.geom.Curve;
import com.sun.javafx.geom.Edge;
import com.sun.javafx.geom.RectBounds;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;
import model.util.*;

public class Controller 
{
	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<VOMovingViolations> queue;

	/**
	 * Tiempos de ejecuci�n
	 */
	long t1, t2;

	/**
	 * Crea una instancia de la clase
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		//inicializar las estructuras de datos
		queue = new Queue<>();
	}

	// M�todo run
	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean loaded = false;
		boolean fin = false;

		while (!fin) 
		{
			view.printMenu();
			int option = sc.nextInt();
			switch (option) 
			{
			case 0:
				fin = true;

			case 1: // CARGAR
				if (!loaded) 
				{
					t1 = System.currentTimeMillis();
					loadMovingViolations();
					t2 = System.currentTimeMillis();
					
					System.out.println("Tiempo de carga: " + (t2-t1));
					loaded = true;
				} 
				else 
				{
					System.out.println();
					System.out.println("Los datos ya fueron cargados (S�lo se pueden cargar una vez)");
					System.out.println();
					break;
				}
			}
		}
		sc.close();
		
		
	}

	//LOAD
	public void loadMovingViolations()
	{

	}
}
