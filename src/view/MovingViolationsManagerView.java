package view;

import model.data_structures.IQueue;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{
		
	}

	public void printMenu() 
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos------------");
		System.out.println("---------------------Taller 8----------------------");
		//EXIT
		System.out.println("0. Salir");
		//EXIT
		//LOAD
		System.out.println("1. Cargar datos");
		//LOAD
	}

	public void printMovingViolations(IQueue<VOMovingViolations> violations) 
	{
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.getObjectId() + " " 
					+ violation.getTicketIssueDate() + " " 
					+ violation.getLocation()+ " " 
					+ violation.getViolationDescription());
		}
	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}

}
