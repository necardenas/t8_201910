package model.data_structures;

import java.util.Iterator;

/******************************************************************************
 *  Compilation:  javac Graph.java        
 *  Execution:    java Graph input.txt
 *  Dependencies: Bag.java Stack.java In.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/41graph/tinyG.txt
 *                https://algs4.cs.princeton.edu/41graph/mediumG.txt
 *                https://algs4.cs.princeton.edu/41graph/largeG.txt
 *
 *  A graph, implemented using an array of sets.
 *  Parallel edges and self-loops allowed.
 *
 *  % java Graph tinyG.txt
 *  13 vertices, 13 edges 
 *  0: 6 2 1 5 
 *  1: 0 
 *  2: 0 
 *  3: 5 4 
 *  4: 5 6 3 
 *  5: 3 4 0 
 *  6: 0 4 
 *  7: 8 
 *  8: 7 
 *  9: 11 10 12 
 *  10: 9 
 *  11: 9 12 
 *  12: 11 9 
 *
 *  % java Graph mediumG.txt
 *  250 vertices, 1273 edges 
 *  0: 225 222 211 209 204 202 191 176 163 160 149 114 97 80 68 59 58 49 44 24 15 
 *  1: 220 203 200 194 189 164 150 130 107 72 
 *  2: 141 110 108 86 79 51 42 18 14 
 *  ...
 *  
 ******************************************************************************/

import java.util.NoSuchElementException;

/**
 *  The {@code Graph} class represents an undirected graph of vertices
 *  named 0 through <em>V</em> � 1.
 *  It supports the following two primary operations: add an edge to the graph,
 *  iterate over all of the vertices adjacent to a vertex. It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  By convention, a self-loop <em>v</em>-<em>v</em> appears in the
 *  adjacency list of <em>v</em> twice and contributes two to the degree
 *  of <em>v</em>.
 *  <p>
 *  This implementation uses an adjacency-lists representation, which 
 *  is a vertex-indexed array of {@link Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the vertices adjacent to a given vertex, which takes
 *  time proportional to the number of such vertices.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/41graph">Section 4.1</a>
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class Graph<Key, VerValue, EdgeValue> {
	
	public class Vertex{
		private Key id;
		private VerValue info;
		private Edge edge;
		
		public Vertex(Key i, VerValue v) {
			id = i; info = v;
			edge = null;
		}

		public Key getId() {
			return id;
		}

		public VerValue getInfo() {
			return info;
		}
		
		public Edge getEdge()
		{
			return edge;
		}

		public void setId(Key id) {
			this.id = id;
		}

		public void setInfo(VerValue info) {
			this.info = info;
		}
		
		public void setEdge(Edge e)
		{
			edge = e;
		}
	}
	
	public class Edge{
		private Vertex v1;
		private Vertex v2;
		private EdgeValue info;
		
		public Vertex getV1() {
			return v1;
		}
		
		public Vertex getV2() {
			return v2;
		}
		
		public EdgeValue getInfo() {
			return info;
		}

		public void setV1(Vertex v1) {
			this.v1 = v1;
		}

		public void setV2(Vertex v2) {
			this.v2 = v2;
		}

		public void setInfo(EdgeValue info) {
			this.info = info;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((info == null) ? 0 : info.hashCode());
			result = prime * result + ((v1 == null) ? 0 : v1.hashCode());
			result = prime * result + ((v2 == null) ? 0 : v2.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Edge other = (Edge) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (info == null) {
				if (other.info != null)
					return false;
			} else if (!info.equals(other.info))
				return false;
			if (v1 == null) {
				if (other.v1 != null)
					return false;
			} else if (!v1.equals(other.v1))
				return false;
			if (v2 == null) {
				if (other.v2 != null)
					return false;
			} else if (!v2.equals(other.v2))
				return false;
			return true;
		}

		private Graph getOuterType() {
			return Graph.this;
		}
	}
	
    private static final String NEWLINE = System.getProperty("line.separator");

    private LinearProbingHashST<Vertex, Bag<Vertex>> adj;
    private LinearProbingHashST<Key, Vertex> vertices;
    private int numVertices;
    private int numEdges;
    
    /**
	 * Construct empty Graph
	 */
	public Graph() {
		adj = new LinearProbingHashST<>();
		vertices = new LinearProbingHashST<>();
		numVertices = numEdges = 0;

	}

	/**
	 * Add a new vertex name with no neighbors (if vertex does not yet exist)
	 * 
	 * @param name
	 *            vertex to be added
	 */
	public Vertex addVertex(Key name, VerValue value) {
		Vertex v;
		v = vertices.get(name);
		if (v == null) {
			v = new Vertex(name, value);
			vertices.put(name, v);
			adj.put(v, new Bag<Vertex>());
			numVertices += 1;
		}
		return v;
	}
	
	/**
	 * Add to to from's set of neighbors, and add from to to's
	 * set of neighbors. Does not add an edge if another edge
	 * already exists
	 * @param from the name of the first Vertex
	 * @param to the name of the second Vertex
	 */
	public void addEdge(Key from, Key to, EdgeValue value) {
		Vertex v, w;
		if (hasEdge(from, to))
			return;
		numEdges += 1;
		if ((v = getVertex(from)) == null)
			return;
		if ((w = getVertex(to)) == null)
			return;
		adj.get(v).add(w);
		adj.get(w).add(v);
	}
	
	public Vertex getVertex(Key name) {
		return vertices.get(name);
	}
	
	public boolean hasEdge(Key id1, Key id2)
	{
		if (!hasVertex(id1) || !hasVertex(id2))
			return false;
		return adj.get(vertices.get(id1)).);
	}
	
	public boolean hasVertex(Key name) {
		return vertices.contains(name);
	}
	
	/**
	 * Returns the Vertex matching v
	 * @param name a String name of a Vertex that may be in
	 * this Graph
	 * @return the Vertex with a name that matches v or null
	 * if no such Vertex exists in this Graph
	 */
	public VerValue getInfoVertex(Key name) {
		return vertices.get(name).getInfo();
	}
	
	/**
	 * Sets the vertex's info
	 * @param id
	 * @param value
	 */
	public void setInfoVertex(Key id, VerValue value)
	{
		vertices.get(id).setInfo(value);
	}
	
	public EdgeValue getInfoArc(Key idIni, Key idFin) throws Exception
	{
		Vertex v1 = vertices.get(idIni), v2 = vertices.get(idFin);
		if (v1.getEdge().equals(v2.getEdge()))
		{
			return v1.getEdge().info;
		}
		else
		{
			throw new Exception("Edge doesn't exist");
		}
	}
	
	public void setInfoArc(Key idVertexIni, Key idVertexFin, EdgeValue infoArc) throws Exception
	{
		Vertex v1 = vertices.get(idVertexIni), v2 = vertices.get(idVertexFin);
		if (v1.getEdge().equals(v2.getEdge()))
		{
			v1.getEdge().setInfo(infoArc);
		}
		else
		{
			throw new Exception("Edge doesn't exist");
		}
	}
	
	public Iterator<Key> adj(Key id)
	{
		return (Iterator<Key>) adj.get(vertices.get(id)).iterator();
	}
}
