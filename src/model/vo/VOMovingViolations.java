package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{
	private int objectID;

	private String location;

	private int addressID;

	private double streetSegID;

	private double xCord;

	private double yCord;

	private String ticketType;

	private int fineamt;

	private double totalPaid;

	private int penalty1;

	private String accidentIndicator;

	private String ticketIssueDate;

	private String violationCode;

	private String violationDesc;

	//Additional atributes
	private Fecha date;
	private Hora hour;

	public VOMovingViolations(int objectId, String location, int adress, double streetSegId, double xCord, double yCord,
			String ticketType, int fineamt, double totalPaid, int penalty1, String accidentIndicator,
			String ticketIssueDate, String violationCode, String violationDesc) 
	{
		this.objectID = objectId;
		this.location = location;
		this.addressID = adress;
		this.streetSegID = streetSegId;
		this.xCord = xCord;
		this.yCord = yCord;
		this.ticketType = ticketType;
		this.fineamt = fineamt;
		this.totalPaid = totalPaid;
		this.penalty1 = penalty1;
		this.accidentIndicator = accidentIndicator;
		this.ticketIssueDate = ticketIssueDate;
		this.violationCode = violationCode;
		this.violationDesc = violationDesc;

		int year = Integer.parseInt(ticketIssueDate.substring(0, 4)); 
		int month = Integer.parseInt(ticketIssueDate.substring(5, 7)); 
		int day = Integer.parseInt(ticketIssueDate.substring(8, 10));
		date = new Fecha(year, month, day);
		int hor = Integer.parseInt(ticketIssueDate.substring(11,13)); 
		int minute = Integer.parseInt(ticketIssueDate.substring(14, 16));
		int seg = Integer.parseInt(ticketIssueDate.substring(17, 19));
		hour = new Hora(hor, minute, seg);
	}

	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public int getObjectId() {
		return objectID;
	}	


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public double getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String getViolationDescription() {
		return violationDesc;
	}

	public int getFineAmt() {
		return fineamt;
	}

	public String getViolationCode() {
		return violationCode;
	}

	public int getPenalty1() {
		return penalty1;
	}

	public int getAddress() {
		return addressID;
	}

	public int getHour() {
		return hour.getHora();
	}

	public double getXCord() {
		return xCord;
	}
	
	public double getYCord() {
		return yCord;
	}
	
	public double getStreetSegId()
	{
		return streetSegID;
	}
	
	public Fecha getDate() {
		return date;
	}
	
	public Hora getHourObject() {
		return hour;
	}
	
	public String toString()
	{
		return objectID + "; " + location + "; " + ticketIssueDate + "; " + violationCode + "; " + fineamt;
	}

	@Override
	public int compareTo(VOMovingViolations that) {	
		int cmp1 = this.date.compareTo(that.date);
		if (cmp1 != 0)
			return cmp1;
		else
		{
			int cmp2 = this.hour.compareTo(that.hour);
			return cmp2;
		}
	}

}
